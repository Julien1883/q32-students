package itis.q32;


/*
В папке resources находятся два .csv файла.
Один содержит данные о группах в университете в следующем формате: ID группы, название группы, код группы
Второй содержит данные о студентах: ФИО, дата рождения, айди группы, количество очков рейтинга

напишите код который превратит содержимое файлов в обьекты из пакета "entities", выведите в консоль всех студентов,
в читабельном виде, с информацией о группе
Используя StudentService, выведите:

1. Число групп с только совершеннолетними студентами
2. Самую маленькую группу
3. Отношение группа - сумма балов студентов фамилия которых совпадает с заданной строкой
4. Отношения студент - дельта баллов до проходного порога (порог передается параметром),
 сгруппированные по признаку пройден порог, или нет

Требования к реализации: все методы в StudentService должны быть реализованы с использованием StreamApi.
Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
*/


import itis.socialtest.parsing.Parser;
import itis2.q32.entities.Student;

import java.util.List;

public class MainClass {


    private StudentService studentService = new StudentServiceImpl();

    public static void main(String[] args) {
        new MainClass().run(
                "src/itis2/q32/resources/groups.csv",
                "src/itis2/q32/resources/students.csv");
    }

    private void run(String studentsPath, String groupsPath) {
        itis2.q32.Parser parser = new itis2.q32.Parser(studentsPath, groupsPath);
        List<Student> students = parser.getAllStudents();
        students.forEach(System.out::println);

    }

}










