package itis.q32;

import itis2.q32.entities.Group;
import itis2.q32.entities.Student;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Parser {


    private final String groupFilepath;
    private final String studentFilepath;
    private final List<Group> groups;

    public Parser(String groupFilepath, String studentFilepath) {
        this.groupFilepath = groupFilepath;
        this.studentFilepath = studentFilepath;
        this.groups= getAllGroups();
    }

    public List<Student> getAllStudents() {
        File file = new File(Paths.get(this.studentFilepath).toUri());
        List<Student> students = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String postLine = scanner.nextLine();
                Student student = deserializeStudent(postLine);
                students.add(student);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return students;
    }

    public List<Group> getAllGroups() {
        File file = new File(Paths.get(this.groupFilepath).toUri());
        List<Group> groups = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String groupLine = scanner.nextLine();
                Group group = deserializeGroup(groupLine);
                groups.add(group);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return groups;
    }

    private Student deserializeStudent(String line) {
        String[] parts = line.split(", ");

        String name = parts[0];
        Integer score = Integer.parseInt(parts[3]);
   //     1869-04-11
        String[] split = parts[1].split("-");
        int i = Integer.parseInt (split[0]);
        int j = Integer.parseInt (split[1]);
        int k = Integer.parseInt (split[2]);
        LocalDate date = LocalDate.of(i,j,k);
        Group group = this.groups.stream()
                .filter(gr1 -> gr1.getId().equals(Integer.parseInt(parts[2])))
                .findFirst().orElse(null);

        return new Student(name,group,score,date);
    }

    private Group deserializeGroup(String line) {
        String[] parts = line.split(", ");
        return new Group(
        parts[1], Long.parseLong(parts[0]), parts[2]
        );
    }
}
