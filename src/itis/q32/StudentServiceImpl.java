package itis.q32;


import itis2.q32.entities.Group;
import itis2.q32.entities.Student;


import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService {


    @Override
    public Group getSmallestGroup(List<Student> students) {
//        Map<Group, Long> map = students.stream().collect(Collectors.toMap(Student::getGroup, Long::sum));
//        return map.keySet().stream()
//                .sorted((key1, key2) -> (int) (map.get(key2) - map.get(key1)))
//                .map(Student::getGroup).findFirst().orElse(null);
        return null;
    }

    @Override
    public Integer countGroupsWithOnlyAdultStudents(List<Student> students) {
        List<Student> collect = students.stream()
                .filter(st -> (2021 - st.getBirthdayDate().getYear()) > 17)
                .collect(Collectors.toList());
        return collect.size();
    }

    @Override
    public Map<String, Integer> getGroupScoreSumMap(List<Student> students, String studentSurname) {

        return null;
    }

    @Override
    public Map<Boolean, Map<String, Integer>> groupStudentScoreWithThreshold(List<Student> students, Integer threshold) {
        return null;
    }
}
